package reposirory;

import java.io.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        super();
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList();
        // объявили переменные для доступа
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            // создали читалку на основе файла
            reader = new FileReader(fileName);
            // создали буфферезированную читалку
            bufferedReader = new BufferedReader(reader);
            // прочитали строку
            String line = bufferedReader.readLine();
            // пока к нам не пришла нулевая строка
            while (line != null) {
                // разбиваем ее по такому символу |
                String[] parts = line.split("\\|");
                System.out.println(line);
                System.out.println(parts.length);
                // берем имя
                String name = parts[0];
                // берем возраст
                int age = Integer.parseInt(parts[1]);
                // берем статус в работе
                boolean isWorker = Boolean.parseBoolean(parts[2]);
                // Создаем нового человека

                User newUser = new User(name, age, isWorker);
                // добавляем его в список
                users.add(newUser);
                //}
                // считываем новую строку
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // finally гарантирует, что этот блок выполнится точно
            if (reader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    // пытаемся закрыть ресурсы. Если будет ошибка,
                    // то просто ее игнарируем. Мы ничего не смогли сделать
                    bufferedReader.close();
                } catch (IOException ignore) {
                }

            }
        }

        return users;
    }

    @Override
    public void save(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, true);
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> userList = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            FileReader fileReader = new FileReader(this.fileName);
            bufferedReader = new BufferedReader(fileReader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int ageFilter = Integer.parseInt(parts[1]);
                if (age == ageFilter) {
                    User newUser = new User(parts[0], age, Boolean.parseBoolean(parts[2]));

                    userList.add(newUser);
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException o) {
            throw new IllegalArgumentException(o);
        } finally {
            // finally гарантирует, что этот блок выполнится точно
            if (reader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }

            }
        }


        return userList;
    }


    @Override
    public List<User> findAllIsWorker() {
        List<User> userList = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            FileReader fileReader = new FileReader(this.fileName);
            bufferedReader = new BufferedReader(fileReader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                boolean isWorker = Boolean.parseBoolean(parts[2]);
                if (isWorker) {
                    User newUser = new User(parts[0], Integer.parseInt(parts[1]), isWorker);
                    userList.add(newUser);
                }
                line = bufferedReader.readLine();
            }

        } catch (IOException o) {
            throw new IllegalArgumentException(o);
        } finally {
            // finally гарантирует, что этот блок выполнится точно
            if (reader != null) {
                try {

                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return userList;
        }
}