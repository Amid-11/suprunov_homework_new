package reposirory;

import java.util.List;



public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();
        for (User user : users) {
            System.out.println(user.getName() + " " + user.getAge() + " " + user.isWorker());
        }

        System.out.println("Find by age:");
        List<User> usersByAge = usersRepository.findByAge(33);
        for (User user : usersByAge) {
            System.out.println(user.getName() + " " + user.getAge() + " " + user.isWorker());
        }

        System.out.println("Find only workers:");
        List<User> usersIsWorker = usersRepository.findAllIsWorker();
        /*for (int i = 0; i < usersIsWorker.size(); i++) {
            User currentUser=usersIsWorker.get(i);
            System.out.println(currentUser.getAge() + " " + currentUser.getName() + " " + currentUser.isWorker());
        }*/

        for (User user : usersIsWorker) {
            System.out.println(user.getName() + " " + user.getAge() + " " + user.isWorker());
        }

        User user = new User("Игорь", 33, true);
        usersRepository.save(user);



    }

}
