package cars;

import java.util.Locale;

public class Auto {
    private String color;
    private int mileage;
    private String numer;
    private int price;
    private String model;

    public String getNumer() {
        return numer;
    }

    public void setNumer(String numer) {
        this.numer = numer;
    }



    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }



    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }



    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public Auto(String numer, String model, String color, int mileage, int price) {
        this.numer = numer;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}

